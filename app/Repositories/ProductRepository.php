<?php

namespace App\Repositories;
use App\Models\Product;

class ProductRepository 
{
  protected $model;
  public function __construct(Product $model)
  {
    $this->model = $model;
  }

  public function createOne(string $name, int $price)
  {
    $this->model->name = $name;
    $this->model->price = $price;
    $this->model->save();
  }
}